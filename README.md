# MAT406(Mathematical Introduction to Game Theory)-Program
A computer program that takes as an input a matrix (of a  zero-sum game) of arbitrary size and gives as an output the reduction of this matrix once all domination by pure column and rows have been taken care of.

## GETTING STARTED
You can either choose to run the python file "Program.py" (if you have python installed, NumPy and Tkinter libraries are required) or the "Program.exe" file in the dist folder.<br/>

Mac/Linux users will have to run the python file!<br/>

## How to use the program
The program is pretty simple to use. There will be two text boxes. The upper one is for you to enter the matrix, the lower one is for outputs (reduction process)

<br/>
<img src="./rdm/1.png"/>
<br/>

1. Enter the matrix in the upper text box (make sure to type your number into the text box) <br/>
2. Separate the numbers by 1 space <br/>
3. Each row needs to be put into a new line <br/>
4. Click the button "Remove Dominated Strategies" <br/>
5. The solution with the process will appear in the lower text box. <br/>

An example could be:
<br/>
<img src="./rdm/2.png"/>
<br/>

## Still thinking about how to remove strategies dominated by mixture of other strategies
<br/>
<img src="./rdm/3.png"/>
<br/>
